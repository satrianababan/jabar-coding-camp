// Soal 1
function next_date(tanggal, bulan, tahun){
    if (bulan == 1){
        bulan = "Januari"
        if (tanggal>0 && tanggal<31){
            tanggal = tanggal + 1
        } else if (tanggal == 31){
            bulan = "Februari"
            tanggal = 1
        }
    } else if (bulan == 2){
        bulan = "Februari"
        if (tahun%4 === 0){
            if (tanggal>0 && tanggal<29){
                tanggal += 1
            } else if (tanggal == 29){
                tanggal = 1
                bulan = "Maret"
            }

        } else {
            if (tanggal>0 && tanggal<28){
                tanggal += 1              
            } else if (tanggal == 28){
                tanggal = 1
                bulan = "Maret"
            }
        }
    } else if (bulan == 3){
        bulan = "Maret"
        if ( tanggal>0 && tanggal<31){
            tanggal += 1
        }else if (tanggal == 31){
            tanggal = 1
            bulan = "April"
        }
    } else if (bulan == 4){
        bulan = "April"
        if ( tanggal>0 && tanggal<30){
            tanggal += 1
        } else if (tanggal == 30){
            tanggal = 1
            bulan = "Mei"
        }
    } else if (bulan == 5){
        bulan = "Mei"
        if ( tanggal>0 && tanggal<31){
            tanggal += 1
        }else if (tanggal == 31){
            tanggal = 1
            bulan = "Juni"
        }
    } else if (bulan == 6){
        bulan = "Juni"
        if ( tanggal>0 && tanggal<30){
            tanggal += 1
        }else if (tanggal == 30){
            tanggal = 1
            bulan = "Juli"
        } 
    } else if (bulan == 7){
        bulan = "Juli"
        if ( tanggal>0 && tanggal<31){
            tanggal += 1
        }else if (tanggal == 31){
            tanggal = 1
            bulan = "Agustus"
        }
    } else if (bulan == 8){
        bulan = "Agustus"
        if ( tanggal>0 && tanggal<31){
            tanggal += 1
        }else if (tanggal == 31){
            tanggal = 1
            bulan = "September"
        }
    } else if (bulan == 9){
        bulan = "September"
        if ( tanggal>0 && tanggal<30){
            tanggal += 1
        }else if (tanggal == 30){
            tanggal = 1
            bulan = "Oktober"
        }
    } else if (bulan == 10){
        bulan = "Oktober"
        if ( tanggal>0 && tanggal<31){
            tanggal += 1
        } else if (tanggal == 31){
            tanggal = 1
            bulan = "November"
        }
    } else if (bulan == 11){
        bulan = "November"
        if ( tanggal>0 && tanggal<30){
            tanggal += 1
        }else if (tanggal == 30){
            tanggal = 1
            bulan = "Desember"
        }
    } else if (bulan == 12){
        bulan = "Desember"
        if ( tanggal>0 && tanggal<31){
            tanggal += 1
        }else if (tanggal == 31){
            tanggal = 1
            bulan = "Januari"
            tahun += 1
        }
   }
   return tanggal + " " + bulan +" "+ tahun
}
var tanggal = 29
var bulan = 12
var tahun = 2020
var ask = next_date(tanggal, bulan, tahun)
console.log(ask)

// Soal 2
function jumlah_kata(a){
    var jumlah = 0
    var spasi = " "
    for (let kata of a.toLowerCase()){
        if (spasi.includes(kata)){
            jumlah++
        }
    }
    return jumlah + 1
}
var kalimat_1 = "Halo nama wwyb wdjnj dbdbnj idjsdhbd uwhdwgwyd wdbhgdy"
var ask = jumlah_kata(kalimat_1)
console.log(ask)