// Soal 1
const luasPersegiPanjang = () => {
    let panjang = 2
    let lebar = 3
    luas = panjang * lebar
    return luas
}
var ask = luasPersegiPanjang()
console.log(ask)

const kelilingPersegiPanjang = () => {
    let panjang = 2
    let lebar = 3
    keliling = 2 * (panjang + lebar)
    return keliling
}
var ask = kelilingPersegiPanjang()
console.log(ask)

// Soal 2
const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName : (firstName + " " + lastName)
    }
}
var ask = newFunction("William", "Imoh").fullName
console.log(ask)

// Soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }
const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)

// Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
console.log(combined)

// Soal 5
const planet = "earth" 
const view = "glass"
console.log(`Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`)