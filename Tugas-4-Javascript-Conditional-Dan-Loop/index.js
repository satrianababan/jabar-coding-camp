// soal 1
var nilai = 75;
if (nilai>=85) {
    console.log("A")
}   else if (nilai>=75 && nilai<85) {
    console.log("B")
}   else if (nilai>=65 && nilai<75) {
    console.log("C")
}   else if (nilai>=55 && nilai<65) {
    console.log("D")
}   else {
    console.log("E")
}


// soal 2
var tanggal = 28;
var bulan = 10;
var tahun = 2002;

switch(bulan) {
    case 1:   { console.log(tanggal + ' '+ 'Januari' + ' '+ tahun); break; }
    case 2:   { console.log(tanggal + ' '+ 'Februari' + ' '+ tahun); break; }
    case 3:   { console.log(tanggal + ' '+ 'Maret' + ' '+ tahun); break; }
    case 4:   { console.log(tanggal + ' '+ 'April' + ' '+ tahun); break; }
    case 5:   { console.log(tanggal + ' '+ 'Mei' + ' '+ tahun); break; }
    case 6:   { console.log(tanggal + ' '+ 'Juni' + ' '+ tahun); break; }
    case 7:   { console.log(tanggal + ' '+ 'Juli' + ' '+ tahun); break; }
    case 8:   { console.log(tanggal + ' '+ 'Agustus' + ' '+ tahun); break; }
    case 9:   { console.log(tanggal + ' '+ 'September' + ' '+ tahun); break; }
    case 10:  { console.log(tanggal + ' '+ 'Oktober' + ' '+ tahun); break; }
    case 11:  { console.log(tanggal + ' '+ 'November' + ' '+ tahun); break; }
    case 12:  { console.log(tanggal + ' '+ 'Desember' + ' '+ tahun); break; }
    default:  { console.log('Bukan rentang bulan'); }
}

//soal 3
console.log('output untuk n=3');
var segitiga = '';
for (var n=1; n<=3 ; n++){
    for (var m=1; m<=n; m++){
        segitiga += "#";
    }
    segitiga += '\n';
}
console.log(segitiga)

console.log('output untuk n=7');
var segitiga = '';
for (var n=1; n<=7 ; n++){
    for (var m=1; m<=n; m++){
        segitiga += "#";
    }
    segitiga += '\n';
}
console.log(segitiga)

// soal 4
var str1 = ' - I love programming'
var str2 = ' - I love Javascript'
var str3 = ' - I love VueJS'
var batas = ''
for (var m=1; m<=10; m++){
    if (m%3 == 0){
        console.log(m + str3)
    } else if (m%2 == 1){
        console.log(m + str1)
    } else if (m%2 == 0) {
        console.log(m + str2)
        }
        if (m%3 == 0){
            batas += "==="
            console.log(batas)
            }
}