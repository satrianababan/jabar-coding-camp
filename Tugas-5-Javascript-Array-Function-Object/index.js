// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var str = daftarHewan.sort()

for (m=0; m<5; m++) {
    if (m == 0){
        console.log(str[0])
    } else if (m == 1) {
        console.log(str[1])
    } else if (m == 2){
        console.log(str[2])
    } else if (m == 3){
        console.log(str[3])
    } else if (m == 4) {
        console.log(str[4])
    }
}

// soal 2
var data = {name : "Satria Octavianus Nababan" , age : 19 , address : "Jalan Cisitu Lama", 
hobby : "Olahraga" }

function introduce(){
    return ("Nama saya "+ data.name+ " umur saya "+ data.age +" tahun" +" alamat saya di "+ data.address+ ", dan saya punya hobby yaitu "+data.hobby)
}
var data = {name : "Satria Octavianus Nababan" , age : 19 , address : "Jalan Cisitu Lama", 
hobby : "olahraga" }
var perkenalan = introduce(data)
console.log(perkenalan)

// soal 3
var vokal = ["a", "i", "u", "e", "o"]
function hitung_huruf_vokal(str){
    jumlah = 0
    for (let kata of str.toLowerCase()) {
        if (vokal.includes(kata)) {
            jumlah++;
        }
        
    }
    return jumlah
}
var hitung_1 = hitung_huruf_vokal("Satria")
var hitung_2 = hitung_huruf_vokal("bapapapapapa")

console.log(hitung_1, hitung_2)


// soal 4
function hitung(int){
    return int + int - 2
 }

 console.log( hitung(0) ) // -2
 console.log( hitung(1) ) // 0
 console.log( hitung(2) ) // 2
 console.log( hitung(3) ) // 4
 console.log( hitung(-1) ) // 8